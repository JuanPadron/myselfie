﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Selfies
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MediaCapture _mediaCapture;
        private DispatcherTimer timer = new DispatcherTimer();

        Boolean playback = false;
        
        public List<String> arrayImages = new List<string>();
        public int indexImage = -1;
        
        public MainPage()
        {
            InitializeComponent();
            DataContext = this; 
            Application.Current.Resuming += Application_Resuming;
            timer.Interval = new TimeSpan(0, 0, 10);
            timer.Tick += TimerTick;
        }

        private async void Application_Resuming(object sender, object o)
        {
            await InitializeCameraAsync();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await InitializeCameraAsync();
            
        }

        private async Task InitializeCameraAsync()
        {
            if (_mediaCapture == null)
            {
                // Get the camera devices
                var cameraDevices = await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture);
                var backFacingDevice = cameraDevices
                    .FirstOrDefault(c => c.EnclosureLocation?.Panel == Windows.Devices.Enumeration.Panel.Back);
                var preferredDevice = backFacingDevice ?? cameraDevices.FirstOrDefault();
                _mediaCapture = new MediaCapture();
                await _mediaCapture.InitializeAsync(new MediaCaptureInitializationSettings{
                        VideoDeviceId = preferredDevice.Id});
                PreviewControl.Source = _mediaCapture;
                await _mediaCapture.StartPreviewAsync();
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            int ran = new Random().Next();
            var storageFolder = KnownFolders.SavedPictures;
            var file = await storageFolder.CreateFileAsync($"{ran}.jpg" , CreationCollisionOption.ReplaceExisting);
            arrayImages.Add(file.Path);
            await _mediaCapture.CapturePhotoToStorageFileAsync(ImageEncodingProperties.CreateJpeg(), file);
        }
        

        private void TimerTick(object sender, Object e)
        {
            if (indexImage >= 0 && indexImage <= arrayImages.Count)
            {
                Uri imageuri = new Uri(arrayImages[indexImage], UriKind.Absolute);
                BitmapImage imagebitmap = new BitmapImage(imageuri);
                ImageDisplay.Source = imagebitmap;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
            {
            if (playback == false)
            {
                playback = true;
                PreviewControl.Visibility = Visibility.Collapsed;
                ImageDisplay.Visibility = Visibility.Visible;
                timer.Start();
            }
            else if (playback ==  true)
            {
                playback = false;
                ImageDisplay.Visibility = Visibility.Collapsed;
                PreviewControl.Visibility = Visibility.Visible;
                timer.Start();
            }
        }
    }
}
